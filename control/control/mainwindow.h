#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "boardinterface.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT
    BoardInterface *interface;
public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void on_verticalScrollBar_2_valueChanged(int value);

    void on_plainTextEdit_textChanged();

    void on_verticalScrollBar_valueChanged(int value);

    void on_verticalScrollBar_3_valueChanged(int value);

    void on_verticalScrollBar_4_valueChanged(int value);

    void on_verticalScrollBar_5_valueChanged(int value);

    void on_verticalScrollBar_6_valueChanged(int value);

    void on_verticalScrollBar_7_valueChanged(int value);

    void on_verticalScrollBar_8_valueChanged(int value);

private:
    Ui::MainWindow *ui;
};

#endif // MAINWINDOW_H
