#ifndef BOARDINTERFACE_H
#define BOARDINTERFACE_H

#include <QObject>
#include <QtSerialPort/QSerialPort>
#include <QTimer>

#define readCommand 1
#define writeCommand 2
#define responseOk 0
#define responseError 1

#define readCommandLength 3
#define writeCommandLength 4

#define lengthIndex 0
#define responseCodeIndex 1

typedef struct {
    uint8_t channel;
    uint8_t value;
} tCommand;

class BoardInterface : public QObject
{
    Q_OBJECT
    QSerialPort *port;
    QTimer *reconnect, *sendCooldown, *replyTimeout;
    bool reconnectionInProgress = false;
    bool sendInProgress = false;
    QList <tCommand> queue;
    QByteArray receiveBuffer;
    void requestReconnectPort();
public:
    explicit BoardInterface(QObject *parent = nullptr);
    setPort(QString port);
    queueWrite(uint8_t channel, uint8_t value);
    log(QString message);

signals:

public slots:
    void handleRead();
    void handleError(QSerialPort::SerialPortError error);
    void reconnectPort();
    void processQueue();
    void timeout();
};

#endif // BOARDINTERFACE_H
