#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "boardinterface.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    this->interface = new BoardInterface(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_verticalScrollBar_2_valueChanged(int value)
{
    this->interface->queueWrite(1, value);
}

void MainWindow::on_plainTextEdit_textChanged()
{
    this->interface->setPort(ui->plainTextEdit->toPlainText());
}

void MainWindow::on_verticalScrollBar_valueChanged(int value)
{
    this->interface->queueWrite(0, value);
}

void MainWindow::on_verticalScrollBar_3_valueChanged(int value)
{
    this->interface->queueWrite(2, value);
}

void MainWindow::on_verticalScrollBar_4_valueChanged(int value)
{
    this->interface->queueWrite(3, value);
}

void MainWindow::on_verticalScrollBar_5_valueChanged(int value)
{
    this->interface->queueWrite(4, value);
}

void MainWindow::on_verticalScrollBar_6_valueChanged(int value)
{
    this->interface->queueWrite(5, value);
}

void MainWindow::on_verticalScrollBar_7_valueChanged(int value)
{
    this->interface->queueWrite(6, value);
}

void MainWindow::on_verticalScrollBar_8_valueChanged(int value)
{
    this->interface->queueWrite(7, value);
}
