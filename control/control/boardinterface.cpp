#include "boardinterface.h"
#include <QtSerialPort/QSerialPort>
#include <QTimer>
#include <iostream>

void BoardInterface::requestReconnectPort()
{
    log("reconnect port requested");
    if (reconnectionInProgress) return;
    reconnectionInProgress = true;
    reconnect->start();
}

BoardInterface::BoardInterface(QObject *parent) : QObject(parent)
{
    this->port = new QSerialPort(this);
    port->setBaudRate(QSerialPort::Baud19200);
    port->setDataBits(QSerialPort::Data8);
    port->setParity(QSerialPort::NoParity);
    port->setStopBits(QSerialPort::OneStop);
    connect(port, static_cast<void (QSerialPort::*)(QSerialPort::SerialPortError)>(&QSerialPort::error), this, &BoardInterface::handleError);
    connect(port, &QSerialPort::readyRead, this, &BoardInterface::handleRead);
    this->replyTimeout = new QTimer(this);
    replyTimeout->setInterval(2000);
    replyTimeout->setSingleShot(true);
    connect(replyTimeout, &QTimer::timeout, this, &BoardInterface::timeout);
    this->sendCooldown = new QTimer(this);
    sendCooldown->setInterval(100);
    sendCooldown->setSingleShot(true);
    connect(sendCooldown, &QTimer::timeout, this, &BoardInterface::processQueue);
    this->reconnect = new QTimer(this);
    reconnect->setInterval(1000);
    reconnect->setSingleShot(true);
    connect(reconnect, &QTimer::timeout, this, &BoardInterface::reconnectPort);
    requestReconnectPort();
}

BoardInterface::setPort(QString port)
{
    this->port->setPortName(port);
}

BoardInterface::queueWrite(uint8_t channel, uint8_t value)
{
    queue.push_front({channel, value});
    if (! sendInProgress) processQueue();
}

BoardInterface::log(QString message)
{
    std::cout << message.toStdString() << std::endl;
}

void BoardInterface::handleRead()
{
    QByteArray reply = port->readAll();
    QString dump;
    for (uint8_t b: reply) {
        dump.append(QString(" %1").arg(b, 0, 16));
    }
    log(QString("received %1").arg(dump));
    /*receiveBuffer.append(reply);
    while (0 < receiveBuffer.size() && receiveBuffer[0] <= receiveBuffer.size()) {
        // if we've got message with 0 byte length, it's definitely a sync fault.
        // So, wait for timeout, clear buffer and start from scratch
        if (0 == receiveBuffer[0]) {
            receiveBuffer.clear();
            return;
        }
        QByteArray reply = receiveBuffer.left(receiveBuffer[0]);
        receiveBuffer = receiveBuffer.right(receiveBuffer.size() - receiveBuffer[0]);
        replyTimeout->stop();
        if (responseOk == reply[responseCodeIndex]) {
            // reply ok
            log("reply ok");
        } else {
            // reply error
            log(QString("board return error code %1").arg(reply[responseCodeIndex]));
        }
    }*/
    replyTimeout->stop();
    if (! sendCooldown->isActive()) sendCooldown->start();
}

void BoardInterface::handleError(QSerialPort::SerialPortError error)
{
    log(QString("error %1 handled").arg(error));
    if (0 != error) requestReconnectPort();
}

void BoardInterface::reconnectPort()
{
    reconnectionInProgress = false;
    if (port->isOpen()) port->close();
    if (! port->open(QIODevice::ReadWrite)) {
        log("port can not open");
        requestReconnectPort();
        return;
    }
}

/**
 * @brief BoardInterface::processQueue
 * @warning not reentrable! contains non-atomic actions with queue
 * but for this project it's okay
 */
void BoardInterface::processQueue()
{
    if (queue.empty()) {
        sendInProgress = false;
        return;
    }
    sendInProgress = true;
    tCommand c = queue.back();
    queue.pop_back();
    QByteArray message;
    // message length
    message.append(4);
    // command "write"
    message.append(writeCommand);
    // channel
    message.append(c.channel);
    // value
    message.append(c.value);
    QString dump;
    for (uint8_t b: message) {
        dump.append(QString(" %1").arg(b, 0, 16));
    }
    log(QString("sent %1").arg(dump));
    qint64 written = port->write(message);
    if (message.size() != written) {
        log("not all message sent");
        queue.push_back(c);

    }
    replyTimeout->start();
}

void BoardInterface::timeout()
{
    log("timeout");
    receiveBuffer.clear();
    sendInProgress = false;
    requestReconnectPort();
}
