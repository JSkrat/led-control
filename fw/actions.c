#include "actions.h"
#if TARGET == T_TEST
    #include <stdio.h>
#endif // TARGET

void sortLevels(tMasks *newSortedLevels) {
    int nslSize = 0;
    for (int levelIndex = 0; levelIndex < levelsSize; levelIndex++) {
        printf("levelIndex %d, nslSize %d, level %d\n", levelIndex, nslSize, levels[levelIndex]);
        tMasks nslItem;
        nslItem.mask = (1 << levelIndex);
        nslItem.phase = levels[levelIndex];
        if (0 == levelIndex) {
            newSortedLevels[0] = nslItem;
            nslSize = 1;
        } else {
            char nslItemAdded = false;
            for (int nslSearchIndex = 0; nslSearchIndex < nslSize; nslSearchIndex++) {
                    printf("nslSearchIndex %d\n", nslSearchIndex);
                // search for equal or bigger
                if (levels[levelIndex] == newSortedLevels[nslSearchIndex].phase) {
                    printf("equal, add mask\n");
                    newSortedLevels[nslSearchIndex].mask |= nslItem.mask;
                    nslItemAdded = true;
                    break;
                } else if (levels[levelIndex] < newSortedLevels[nslSearchIndex].phase) {
                    // and then insert new at that place, moving all next one up
                    tMasks tmp = nslItem;
                    nslSize++;
                    for (int nslMoveIndex = nslSearchIndex; nslMoveIndex < nslSize; nslMoveIndex++) {
                        printf("nslMoveIndex %d\n", nslMoveIndex);
                        tMasks swap = tmp;
                        tmp = newSortedLevels[nslMoveIndex];
                        newSortedLevels[nslMoveIndex] = swap;
                    }
                    nslItemAdded = true;
                    break;
                }
            }
            // add item at the end if we still not placed it anywhere
            if (! nslItemAdded) {
                newSortedLevels[nslSize++] = nslItem;
            }
        }
    }
    // the last element, in case there are no elements with 0xFF. kinda terminal elemenr
    newSortedLevels[nslSize].mask = 0;
    newSortedLevels[nslSize].phase = 0;
    nslSize++;
}
