/*! @file      hardware.c
 *  @author  - ������� � � - ��������������
 *  @date    - 9/1/2017
 *  @version - 1.0
 *  @brief   - Hardware init
 *  @ide     - IAR
 *  @copyright - 3S LABS
 *
*/

#include "hardware.h"

// Functins
/******************************************************************************/
/******************************************************************************/
/** @brief     - Init all  MCUs periphery
 *  @param[in] - none
 *  @retval    - none
*/
void Hardware_Init(void)
{
  /**
   * @addtogroup Hardware init
   * @{
  */

  /****************************************************************************/
  // Settings GPIO
  // Settings - PORTA
  /****************************************************************************/
  // PA0 - input  - analog input 0
  // PA1 - input  - analog input 1
  // PA2 - input  - button 1 - need Pull Up
  // PA3 - input  - button 2 - need Pull Up
  // PA4 - output - not used
  // PA5 - output - not used
  // PA6 - input  - discrete input 1
  // PA7 - input  - discrete input 2
  PORTA = (1 << PORTA7)|(1 << PORTA6)|(0 << PORTA5)|(0 << PORTA4)|(1 << PORTA3)|(1 << PORTA2)|(0 << PORTA1)|(0 << PORTA0);
  DDRA  =   (0 << PORTA7)|  (0 << PORTA6)|  (1 << PORTA5)|  (1 << PORTA4)|  (0 << PORTA3)|  (0 << PORTA2)| (0 << PORTA1) |(0 << PORTA0);

  // Settings PORTB
  /****************************************************************************/
  // PB0 - output - not used
  // PB1 - output - not used
  // PB2 - output - not used
  // PB3 - output - not used
  // PB4 - output - not used
  // PB5 - output - not used
  // PB6 - output - not used
  // PB7 - output - not used
  PORTB = (0 << PORTB7)|(0 << PORTB6)|(0 << PORTB5)|(0 << PORTB4)|(0 << PORTB3)|(0 << PORTB2)|(0 << PORTB1)|(0 << PORTB0);
  DDRB  =   (1 << PORTB7)|  (1 << PORTB6)|  (1 << PORTB5)|  (1 << PORTB4)|  (1 << PORTB3)|  (1 << PORTB2)| (1 << PORTB1) |(1 << PORTB0);

  // Settings PORTC
  /****************************************************************************/
  // PC0 - output - discrete output 1
  // PC1 - output - discrete output 2
  // PC2 - output - discrete output 3
  // PC3 - output - discrete output 4
  // PC4 - output - discrete output 5
  // PC5 - output - discrete output 6
  // PC6 - output - discrete output 7
  // PC7 - output - discrete output 8
  PORTC = (1 << PORTC7)| (1 << PORTC6)|(1 << PORTC5)| (1 << PORTC4)| (1 << PORTC3)| (1 << PORTC2)| (1 << PORTC1)| (1 << PORTC0);
  DDRC  =   (1 << PORTC7)|   (1 << PORTC6)|(1 << PORTC5)|   (1 << PORTC4)|   (1 << PORTC3)|   (1 << PORTC2)|   (1 << PORTC1)|   (1 << PORTC0);

  // Settings PORTD
  /****************************************************************************/
  // PD0 - input  - RX0 - UART0
  // PD1 - output - TX0 - UART0
  // PD2 - input  - RX1 - UART1
  // PD3 - output - TX1 - UART1
  // PD4 - output - not used
  // PD5 - output - not used
  // PD6 - output - LED1
  // PD7 - output - LED2
  PORTD = (1 << PORTD7)|(1 << PORTD6)| (0 << PORTD5)| (0 << PORTD4)| (1 << PORTD3)| (1 << PORTD2)| (1 << PORTD1)| (1 << PORTD0);
  DDRD  =   (1 << PORTD7)|  (1 << PORTD6)|   (1 << PORTD5)|   (1 << PORTD4)|   (1 << PORTD3)|   (0 << PORTD2)|   (1 << PORTD1)|   (0 << PORTD0);



  // ��������� ������� 0
  /****************************************************************************/
  // ����� - normal
  // ������� ����� - 0.25���
  // �������� ���������� �� 64
  TCCR0A = 0;
  TCCR0B = (0 << CS02) | (1 << CS01) | (1 << CS00);
  TIMSK0 = (1 << OCIE0A) | (0 <<OCIE0B) | (1 << TOIE0);

  // ��������� ������� 1
  /****************************************************************************/
  // ����� - normal
  // ������� ����� - 0.25���
  // �������� ���������� �� 64
  /*TCNT1H = 0xFF;
  TCCR1A = (0 << WGM11) | (0 << WGM10);
  TCCR1B = (0 << WGM13) | (0 << WGM12) | (0 << CS12) | (1 << CS11) | (1 << CS10);
  TIMSK1 = (1 << OCIE1A) | (1 << OCIE1B) | (1 << TOIE1);
  OCR1AH = 0xFF;
  OCR1BH = 0xFF;*/

  // ��������� ������� 2
  /****************************************************************************/
  // ����� - normal
  // ������� ����� - 0.5���
  // �������� ���������� �� 32
  /*TCCR2A = 0;
  TCCR2B = (0 << CS22) | (1 << CS21) | (1 << CS20);
  ASSR = 0;
  TIMSK2 = (1 << OCIE2A) | (1 << OCIE2B) | (1 << TOIE2);*/


  // ��������� ����������� - �� ������������
  /****************************************************************************/
  ACSR = (1 << ACD);


  // ��������� UART0
  /****************************************************************************/
  // �������� - 19200
	// 1 �������� ���
  UCSR0B = 0x00;
  UCSR0A = (1 << U2X0);
  UCSR0C = (0 << USBS0) | (1 << UCSZ01) | (1 << UCSZ00);
	UBRR0L = 103;	//set baud rate lo
	UBRR0H = 0x00;	//set baud rate hi
  UCSR0B = (0 << UDRIE0) | (1 << TXEN0) | (1 << RXEN0) | (1 << RXCIE0);

	 // ��������� UART1 - ������ � �������
  /****************************************************************************/
  // �������� - 19200
	// 1 �������� ���
  UCSR1B = 0x00;
  UCSR1A = (1 << U2X1);
  UCSR1C = (0 << USBS1) | (1 << UCSZ11) | (1 << UCSZ10);
	UBRR1L = 103;	//set baud rate lo
	UBRR1H = 0x00;	//set baud rate hi
  UCSR1B = (0 << TXCIE1) | (0 << TXEN1) | (0 << RXEN1) | (0 << RXCIE1);

	 // ��������� ����������� �������
	 /****************************************************************************/
	 /*__watchdog_reset();
  WDTCSR = (1<<WDCE)|(1<<WDE);
  WDTCSR = (1<<WDE)|(0<<WDP3)|(1<<WDP2)|(0<<WDP1)|(0<<WDP0);
	 __watchdog_reset();*/

  //! Init internal ADC
  /****************************************************************************/
  //ADC_Init();

  /**
   * @}
  */
}

/** @fn - void ADC_Init(void)
 *  @brief     - Init internal ADC module
 *  @param[in] - none
 *  @param[in] - none
 *  @retval    - none
*/
void ADC_Init(void)
{
  //! Init ADC registers
  /****************************************************************************/
	ADCSRA = 0x00;
	ADMUX =  (0 << REFS1) | (1 << REFS0) | (0 << MUX2) | (0 << MUX1) | (0 << MUX0);
	ADCSRA = (1 << ADEN) | (1 << ADSC) | (1 << ADATE) | (1 << ADIE) | (1 << ADPS2) | (1 << ADPS1) | (1 << ADPS0);
	ADCSRB = (0 << ADTS2) | (0 << ADTS1) | (0 << ADTS0);
}

void rxState(char state) {
    if (state) {
        UCSR0B |= (1 << RXEN0);
    } else {
        UCSR0B &= ~(1 << RXEN0);
    }
}

void txState(char state) {
    if (state) {
        UCSR0B |= (1 << UDRIE0);
    } else {
        UCSR0B &= ~(1 << UDRIE0);
    }
}
