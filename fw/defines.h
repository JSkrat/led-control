#ifndef DEFINES_H_INCLUDED
#define DEFINES_H_INCLUDED

/// @warning! active level is zero!
// digital outputs
#define XP1 PORTC0
#define XP5 PORTC1
#define XP7 PORTC2
#define XP8 PORTC3
#define XP10 PORTC4
#define XP13 PORTC5
#define XP14 PORTC6
#define XP11 PORTC7

// analog inputs
#define XP3 PORTA0
#define XP6 PORTA1

#define LEDyellow PORTD6
#define LEDblue PORTD7

#define HL13 PORTD6
#define HL14 PORTD7

#define SW2 PINA2
#define SW3 PINA3

#endif // DEFINES_H_INCLUDED
