/*! @file      hardware.h
 *  @author  - ������� � � - ��������������
 *  @date    - 9/1/2017
 *  @version - 1.0
 *  @brief   - Init hardware
 *  @ide     - IAR
 *  @copyright - 3S LABS
 *
*/
#ifndef HARDWARE_H_
#define HARDWARE_H_

// Headers
/******************************************************************************/
/******************************************************************************/
//#include "main.h"
#include <avr/io.h>


// Macros
/******************************************************************************/
/******************************************************************************/

/// LEDs
/******************************************************************************/
/** @def LED_PROCESS_ON                LED_PROCESS = 0
  * @def LED_PROCESS_OFF               LED_PROCESS = 1
  * @def LED_TEST_ON                   LED_TEST = 0
  * @def LED_TEST_OFF                  LED_TEST = 1
*/

/// Work process LED
#define LED_PROCESS                   PORTD_Bit6
#define LED_PROCESS_ON                LED_PROCESS = 0
#define LED_PROCESS_OFF               LED_PROCESS = 1

/// Test LED
#define LED_TEST                      PORTD_Bit7
#define LED_TEST_ON                   LED_TEST = 0
#define LED_TEST_OFF                  LED_TEST = 1

/**
 * @def VALVE1
 * @def VALVE2
 * @def VALVE3
 * @def VALVE4
 */
//! Control dg output
/******************************************************************************/
#define VALVE1                        PORTC_Bit0
#define VALVE2                        PORTC_Bit1
#define VALVE3                        PORTC_Bit2
#define VALVE4                        PORTC_Bit3

#define PUMP1                         PORTC_Bit4
#define PUMP2                         PORTC_Bit5
#define PUMP3                         PORTC_Bit6
#define PUMP4                         PORTC_Bit7

#define VALVE1_ON                     VALVE1 = 0
#define VALVE2_ON                     VALVE2 = 0
#define VALVE3_ON                     VALVE3 = 0
#define VALVE4_ON                     VALVE4 = 0

#define VALVE1_OFF                    VALVE1 = 1
#define VALVE2_OFF                    VALVE2 = 1
#define VALVE3_OFF                    VALVE3 = 1
#define VALVE4_OFF                    VALVE4 = 1

#define PUMP1_ON                      PUMP1 = 0
#define PUMP2_ON                      PUMP2 = 0
#define PUMP3_ON                      PUMP3 = 0
#define PUMP4_ON                      PUMP4 = 0

#define PUMP1_OFF                     PUMP1 = 1
#define PUMP2_OFF                     PUMP2 = 1
#define PUMP3_OFF                     PUMP3 = 1
#define PUMP4_OFF                     PUMP4 = 1

// Global variables
/******************************************************************************/
/******************************************************************************/


// Functions prototypes
/******************************************************************************/
/******************************************************************************/
/** @fn Hardware_Init(void)
 *  @brief     - Init all  MCUs periphery
 *  @param[in] - none
 *  @retval    - none
*/
void Hardware_Init(void);

/** @fn - void ADC_Init(void)
 *  @brief     - Init internal ADC module
 *  @param[in] - none
 *  @param[in] - none
 *  @retval    - none
*/
void ADC_Init(void);

void rxState(char enabled);
void txState(char enabled);

#endif
