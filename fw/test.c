#include "actions.h"
#include <stdio.h>

tMasks sortedLevels[9];

#define test(condition, message) if (! condition) { printf(message); return 1; }

int main(void) {
    printf("=== levels all 0 ===\n");
    for (int i = 0; i < levelsSize; i++) levels[i] = 0;
    sortLevels(sortedLevels);
    test(sortedLevels[0].phase == 0, "0th element phase is not 0");
    test(sortedLevels[0].mask == 0xFF, "0th element mask is not 0xFF");
    printf("PASS\n");

    printf("=== first level is 10 ===\n");
    levels[0] = 10;
    sortLevels(sortedLevels);
    test(sortedLevels[0].phase == 0, "0th element phase is not 0");
    test(sortedLevels[0].mask == 0xFE, "0th element mask is not 0xFE");
    test(sortedLevels[1].phase == 10, "1st element phase is not 10");
    test(sortedLevels[1].mask == 1, "1st element mask is not 1");
    return 0;
}
