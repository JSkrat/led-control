#ifndef ACTIONS_H_INCLUDED
#define ACTIONS_H_INCLUDED

#if TARGET == T_MC
    #include <avr/io.h>
    #define printf(fmt, ...) (void) 0
#else
    #include <inttypes.h>
#endif // TARGET
#define false 0
#define true 1

/// the actual brightness levels of each of 8 outputs
#define levelsSize 8
uint8_t levels[levelsSize];

typedef struct {
    uint8_t mask;
    uint8_t phase;
} tMasks;

void sortLevels(tMasks *newSortedLevels);

#endif // ACTIONS_H_INCLUDED
