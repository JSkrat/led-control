/*
command frame is
length prefix channel [value]
length is total command length in bytes
prefix 1 means reading channel
prefix 2 means writing channel
channel is from 8 to 15, also one-byte length
value is one byte in case of writing command
and then it stops receiving for parsing

response frame is
result_code [value] stop
result_code 0 means ok
result_code any other means error code
value is for reading command
stop is 4

 */
#define readCommand 1
#define writeCommand 2
#define responseOk 0
#define responseError 1

#define readCommandLength 3
#define writeCommandLength 4

#define lengthIndex 0
#define prefixIndex 1
#define channelIndex 2
#define valueWIndex 3

#include <avr/io.h>
#include <avr/interrupt.h>
#include <avr/sleep.h>
#include "hardware.h"
#include "defines.h"
#include "actions.h"

volatile enum eState {idle, parseCommand, sendOk, sendError, makeList} state;
#define commandSize 64
uint8_t command[commandSize];
unsigned char receiveIndex;
uint8_t response[64];
unsigned char transmitIndex;
unsigned char *transmitBase;

uint8_t delay;
#define channelsSize 9
uint8_t *const channels[channelsSize] = {&levels[0], &levels[1], &levels[2], &levels[3], &levels[4], &levels[5], &levels[6], &levels[7], &delay};

struct {
    uint8_t length;
    tMasks sortedLevels[9];
} sLevels_1, sLevels_2;
tMasks *bufferedCurrentSortedLevels, *currentSortedLevels;
uint8_t indexLevels;

void beginSendError() {
    response[0] = 2;
    response[1] = responseError;
    transmitIndex = 0;
    transmitBase = &response[0];
    txState(1);
}

void beginSendOk() {
    response[0] = 2;
    response[1] = responseOk;
    transmitIndex = 0;
    transmitBase = &response[0];
    txState(1);
}

void beginSendCurrentLevels() {
    transmitIndex = 0;
    transmitBase = (unsigned char*) bufferedCurrentSortedLevels;
    transmitBase--;
    txState(1);
}

int main(void) {
    // initialization
    Hardware_Init();
    set_sleep_mode(SLEEP_MODE_IDLE);
    state = idle;
    receiveIndex = 0;
    currentSortedLevels = &sLevels_1.sortedLevels[0];
    bufferedCurrentSortedLevels = currentSortedLevels;
    indexLevels = 0;
    sLevels_1.length = sizeof(sLevels_1.sortedLevels[0]) * 9;
    sLevels_2.length = sizeof(sLevels_1.sortedLevels[0]) * 9;
    sei();
    while(1) {
        switch (state) {
            default:
            case idle: {
                sleep_mode();
                break;
            }
            case parseCommand: {
                state = sendError;
                // check channel number
                if (channelsSize <= command[channelIndex]) {
                    // wrong channel
                    break;
                }
                if (readCommand == command[prefixIndex]) {
                    // reading channel
                    // not implemented yet (we will need it only when there light sensor will be connected there)
                } else if (writeCommand == command[prefixIndex]) {
                    // writing channel
                    if (writeCommandLength != command[lengthIndex]) break;
                    *(channels[command[channelIndex]]) = command[valueWIndex];
                    receiveIndex = 0;
                    state = sendOk;
                    break;
                }
                break;
            }
            case sendOk: {
                state = makeList;
                beginSendOk();
                break;
            }
            case sendError: {
                state = idle;
                beginSendError();
                break;
            }
            case makeList: {
                PORTD &= (1 << HL14);
                PORTD |= (1 << HL13);
                state = idle;
                // undo switch to the new sorted array if it was
                bufferedCurrentSortedLevels = currentSortedLevels;
                // pick not current
                tMasks *newSortedLevels = &sLevels_1.sortedLevels[0];
                if (currentSortedLevels == newSortedLevels) newSortedLevels = &sLevels_2.sortedLevels[0];
                // create new list � tested function
                sortLevels(newSortedLevels);
                // next overflow we will switch to a new one
                bufferedCurrentSortedLevels = newSortedLevels;
                beginSendCurrentLevels();
                PORTD |= (1 << HL14);
                break;
            }
        }
    }

    return 0;
}

ISR(TIMER0_COMPA_vect) {
    PORTC |= currentSortedLevels[indexLevels].mask;
    OCR0A = currentSortedLevels[++indexLevels].phase;
}

ISR(TIMER0_OVF_vect) {
    currentSortedLevels = bufferedCurrentSortedLevels;
    indexLevels = 0;
    // turn on all
    if (0 == currentSortedLevels[indexLevels].phase) {
        PORTC = currentSortedLevels[indexLevels].mask;
        indexLevels++;
    } else {
        PORTC = 0;
    }
    OCR0A = currentSortedLevels[indexLevels].phase;
}

/// byte received
char rxLength;
ISR(USART0_RX_vect) {
    uint8_t byte = UDR0;
    if (0 == receiveIndex) rxLength = byte;
    command[receiveIndex] = byte;
    receiveIndex++;
    if (0 >= --rxLength) {
        rxState(0);
        state = parseCommand;
    }
}

/// transmitter buffer is empty and ready to receive next byte
char txLength;
ISR(USART0_UDRE_vect) {
    UDR0 = transmitBase[transmitIndex];
    if (0 == transmitIndex) txLength = transmitBase[transmitIndex];
    transmitIndex++;
    if (0 >= --txLength) {
        // we've done with response and ready to receive another command
        rxState(1);
        txState(0);
    }
}
